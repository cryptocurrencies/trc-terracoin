# ---- Base Node ---- #
FROM ubuntu:16.04 AS base
RUN apt-key adv --keyserver keyserver.ubuntu.com --recv-keys 8842ce5e && \
    echo "deb http://ppa.launchpad.net/bitcoin/bitcoin/ubuntu xenial main" > /etc/apt/sources.list.d/bitcoin.list
RUN apt-get update && \
    apt-get install -y build-essential libtool autotools-dev autoconf libssl-dev git-core libboost-all-dev libdb4.8-dev libdb4.8++-dev libevent-dev libminiupnpc-dev pkg-config && \
    apt-get clean && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

# ---- Build Source ---- #
FROM base as build
RUN git clone https://github.com/terracoin/terracoin.git /opt/terracoin && \
    cd /opt/terracoin && \
    ./autogen.sh && \
    ./configure --without-miniupnpc --disable-tests --without-gui --without-qrcode && \
    make -j2

# ---- Release ---- #
FROM ubuntu:16.04 as release
RUN apt-key adv --keyserver keyserver.ubuntu.com --recv-keys 8842ce5e && \
    echo "deb http://ppa.launchpad.net/bitcoin/bitcoin/ubuntu xenial main" > /etc/apt/sources.list.d/bitcoin.list
RUN apt-get update && \
    apt-get install -y  libboost-all-dev libdb4.8 libdb4.8++ libminiupnpc-dev libevent-dev && \
    apt-get clean && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*
RUN groupadd -r terracoin && useradd -r -m -g terracoin terracoin
RUN mkdir /data
RUN chown terracoin:terracoin /data
COPY --from=build /opt/terracoin/src/terracoind /usr/local/bin/terracoind
COPY --from=build /opt/terracoin/src/terracoin-cli /usr/local/bin/terracoin-cli
USER terracoin
VOLUME /data
EXPOSE 13333 13332
CMD ["/usr/local/bin/terracoind", "-conf=/data/terracoin.conf", "-datadir=/data", "-server", "-txindex", "-printtoconsole"]